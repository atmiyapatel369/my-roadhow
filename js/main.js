$(document).ready(function() {   
 
  $('#header').load('header.html');
  $('#footer').load('footer.html');


  // Sticky Header 
  $(window).scroll(function(){
    var sticky = $('header'),
        scroll = $(window).scrollTop();

    if (scroll >= 100) sticky.addClass('fixHeader');
    else sticky.removeClass('fixHeader');
  });
   
   
  /*Scrol to top*/
  $('.scrollTop').on('click',function(){
     $('html, body').animate({scrollTop : 0},800); 
  }); 
});


// Animation Starts
function onScrollInit( items, trigger ) {
  items.each( function() {
    var osElement = $(this),
        osAnimationClass = osElement.attr('data-os-animation'),
        osAnimationDelay = osElement.attr('data-os-animation-delay');
        osElement.css({
          '-webkit-animation-delay':  osAnimationDelay,
          '-moz-animation-delay':     osAnimationDelay,
          'animation-delay':          osAnimationDelay
        });

        var osTrigger = ( trigger ) ? trigger : osElement;
        osTrigger.waypoint(function() {
          osElement.addClass('animated').addClass(osAnimationClass);
          },{
              triggerOnce: true,
              offset: '99%'
        });
  });
}
// onScrollInit( $('.os-animation') ); 
// Animation Ends

 



